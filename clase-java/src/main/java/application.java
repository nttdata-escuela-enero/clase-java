import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.example.ClienteJurico;
import org.example.ClienteNatural;
import org.example.Persona;
import org.example.TipoCliente;

public class application {

  public static void main(String[] args) {

    byte variable1 = 127;

    short variable2 = 32767;

    int variable3 = 123021;

    long variable4 = 1245;

    char variable5 = '1';

    boolean variable6 = true;//false

    double variable7 = 10.001;

    float variable8 = 20.00f;

    String cadena = "1124";
    cadena = cadena + "555";
    cadena += "555";

    Integer varint = 12;

    Double varDbl = 12.00;
    Long varLng = 12L;

    System.out.println(
        "hola mundo!" + variable1 + " - " + variable2 + " - " + variable3 + " - " + variable4 + " - " + variable5 + " - " + variable6
            + " - " + variable7 + " - " + variable8);

    System.out.println(varint.doubleValue());

    Integer nuevoInt = Integer.parseInt(cadena);
    System.out.println(nuevoInt);

    String[] cadena2 = {"hola", "bienvenido"};//0 ->"hola" ,1->"bienvenido"
    System.out.println(cadena2);

    System.out.println(cadena2[1]);

    Persona participante1 = new Persona();
    participante1.nombre = "carloss";
    System.out.println(participante1.nombre);
    System.out.println(participante1.getEdad());
    participante1.setEdad(18);
    System.out.println(participante1.getEdad());

    List<String> listaCadenas = new ArrayList<>();
    listaCadenas.add("elemet1");
    listaCadenas.add("elemet2");
    System.out.println(listaCadenas.get(0));

    System.out.println(listaCadenas);

    Date fechahoy = new Date();

    System.out.println(fechahoy);

    Integer indiceInit = 3;
    Integer indiceFinal = 10;

    boolean resultado;
    if (indiceInit == indiceFinal) {
      resultado = true;
    } else if (indiceInit == 5) {
      resultado = false;
    } else {
      resultado = false;
    }

    System.out.println("resultado es : " + resultado);

    resultado = indiceInit == indiceFinal ? true : false;
    System.out.println("resultado es : " + resultado);

    switch (indiceInit) {
      case 1:
        System.out.println("el indice es 1");
        break;
      case 2:
        System.out.println("el indice es 2");
        break;
      case 3:
      case 4:
        System.out.println("el indice es 3 o 4");
        break;
      default:
        System.out.println("el indice es mayor de 4");
        break;
    }

    for (int i = 0; i < listaCadenas.size(); i++) {
      System.out.println("Valor del " + listaCadenas.get(i));
    }

    for (int i = 0; i < listaCadenas.size(); i++) {
      System.out.println("Valor del " + listaCadenas.get(i));
    }

    for (String elemento : listaCadenas) {
      System.out.println("2 - Valor  del " + elemento);
    }
    System.out.println("inicio del while");

    while (indiceInit < indiceFinal) {
      System.out.println("Valor del indice es : " + indiceInit);
      indiceInit++;
    }

    System.out.println("inicio del do while");

    do {
      System.out.println("Valor del indice es : " + indiceInit);
      indiceInit++;
    } while (indiceInit < indiceFinal);

    ClienteNatural cliente = new ClienteNatural();
    cliente.setDni("12345678");
    cliente.setEdad(28);
    cliente.nombre = "Carloss";

    System.out.println(cliente);

    //    Mamifero mamifero = new Mamifero();

    Persona persona2 = new Persona();
    persona2.setDni("98765432");
    persona2.nombre = "Maria";

    ClienteNatural cliente2 = new ClienteNatural();
    cliente2.setDni("12345679");
    cliente2.setEdad(22);
    cliente2.nombre = "Maria";

    //    si se puede instanciar una clase padre como clase hija
    System.out.println(persona2);
    System.out.println(cliente2);
    Persona persona3 = new ClienteNatural();
    Persona persona4 = new Persona();

    //    Cliente cliente1 = new Persona();no se puede instanciar una clase hija como clase padre

    persona3.mostrarSaludo();
    persona4.mostrarSaludo();

    ClienteNatural clienteNatural = new ClienteNatural();
    clienteNatural.setDni("54658545");
    ClienteJurico clienteJurico = new ClienteJurico();
    clienteJurico.setDni("54658546");

    System.out.println(clienteNatural.getRuc() + " " + clienteNatural.getOfertas());
    System.out.println(clienteJurico.getRuc() + " " + clienteJurico.getOfertas());

    TipoCliente clienteNat = new ClienteNatural();

    ((ClienteNatural) clienteNat).setDni("5555555");

    TipoCliente clienteJur2 = new ClienteJurico();

    System.out.println(clienteNat.getRuc() + " " + ((ClienteNatural) clienteNat).getOfertas() + " " + clienteNat.getSaludo());
    System.out.println(clienteJur2.getRuc() + " " + " " + clienteJur2.getSaludo());

    List<String> nombres = new ArrayList<>();
    nombres.add("Carloss");
    nombres.add("Pedro");
    nombres.add("Luis");
    nombres.add("Jorge");
    nombres.add("Rocio");
    nombres.add("Cristian");

    List<String> nombresFiltrados = new ArrayList<>();
    for (String nombre : nombres) {
      if (nombre.substring(0, 1).equals("C")) {
        nombresFiltrados.add(nombre);
      }
    }
    System.out.println(nombresFiltrados);

    //interfaces funcionales

    //Predicate: condicion y booleano;
    Predicate<String> predicate = (nombre) -> {
      return nombre.substring(0, 1).equals("C");
    };
    //Function: recive un valor y devuelve un valor;
    Function<String, Integer> conversor = nroString -> {
      return Integer.parseInt(nroString);
    };

    //Consumer: recive un parametro pero no devuelve ningun valor ;
    Consumer<String> displayer = (String mensaje) -> {
      System.out.println(mensaje);
    };

    //Supplier: no recive parametros pero si devuelve valor ;
    Supplier<String> proveedor = () -> {
      return "Hola mundo";
    };

    System.out.println(
        "Hola mundo".substring(2, 6));//obtiene el texto desde el indice 0 hasta el indice 2 sin incluir el valor de este ultimo

    // utilizando api stream
    List<String> nombresFiltradosConApiStream =
        nombres.stream().filter((nombre) -> nombre.substring(0, 1).equals("C")).collect(Collectors.toList());

    nombresFiltradosConApiStream.stream().forEach((String mensaje) -> System.out.println(mensaje));

    // utilizando Optional
    ClienteJurico clienteJurico1 = new ClienteJurico();
    Optional<ClienteJurico> clienteJuridico = Optional.ofNullable(clienteJurico1);
    System.out.println(clienteJuridico.orElse(new ClienteJurico("123456", 1, "av. hugas valle")));
    System.out.println(clienteJuridico.orElseThrow(() -> {
      System.out.println("valor nulo");
      return new NullPointerException();
    }));

    List<Integer> numeros = new ArrayList<>();
    numeros.add(1);
    numeros.add(2);
    System.out.println(numeros);

    List<Integer> numerosVer2 = List.of(1, 2);
    System.out.println(numerosVer2);

    //Nuevo tipo de dato var para evitar el tipado de variables.
    var numerosVer3 = "1";
    System.out.println(numerosVer3);

    nombresFiltradosConApiStream.stream().forEach((String mensaje) -> System.out.println(mensaje));
    nombresFiltradosConApiStream.stream().forEach(System.out::println);
    System.out.println(clienteJuridico.orElseGet(() -> {
      System.out.println("Hola mundo");
      return new ClienteJurico();
    }));

    System.out.println(clienteJuridico.orElseGet(ClienteJurico::new));

    List<Integer> listaEnteros = listaCadenas.stream().map(Integer::parseInt).collect(Collectors.toList());

  }
}
