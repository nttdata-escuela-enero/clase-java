package org.example;

public class ClienteNatural extends Persona implements TipoCliente, TipoOfertas {

  private String carnet;

  private Integer role;

  public String getCarnet() {
    return carnet;
  }

  public void setCarnet(String carnet) {
    this.carnet = carnet;
  }

  public Integer getRole() {
    return role;
  }

  public void setRole(Integer role) {
    this.role = role;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  private String direccion;

  public String getDni() {
    return dni;
  }

  public Integer obtenerEdad() {
    return getEdad();
  }

  @Override
  public String toString() {

    return "Cliente{" + "dni: " + dni + " nombre: " + nombre + " edad: " + getEdad() + " nro mamas: " + getNroMamas() + "}";
  }

  @Override
  public void mostrarSaludo() {
    System.out.println("Hola soy Clase Cliente");
  }

  @Override
  public String getRuc() {
    return "10" + dni;
  }

  @Override
  public String getSaludo() {
    return TipoCliente.super.getSaludo() + " desde clase cliente";
  }

  @Override
  public String getOfertas() {
    return "Estas son las ofertas para un cliente Natural";
  }

  @Override
  public String getDescuentos() {
    return "Estas son los descuentos para un cliente Natural";
  }
}
