package org.example;

public class  Persona extends Mamifero {

  public String nombre;

  private Integer edad = 0;

  protected String dni;

  public void setDni(String dni) {
    this.dni = dni;
  }

  public Integer getEdad() {
    return edad;
  }

  public void setEdad(Integer edad) {
    this.edad = edad;
  }

  @Override
  public Integer getNroMamas() {
    return 2;
  }

  public void mostrarSaludo() {
    System.out.println("Hola soy Clase Persona");
  }

  static void metodoPrueba() {
     System.out.println("Probando");
  }
}
