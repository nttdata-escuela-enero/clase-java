package org.example;

public interface TipoOfertas {

  public String getOfertas();

  public String getDescuentos();
}
