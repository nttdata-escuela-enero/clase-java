package org.example;

public enum Categoria {
  CLASE_A("Personas con RUC Juridico","A"),
  CLASE_B("Personas con RUC Natural","B");

  private String descripcion;
  private String acronimo;

  Categoria(String descripcion,String acronimo) {
    this.descripcion = descripcion;
    this.acronimo = acronimo;

  }

  public String getDescripcion() {
    return descripcion;
  }

  public String getAcronimo() {
    return acronimo;
  }
}
