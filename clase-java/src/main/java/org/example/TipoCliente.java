package org.example;

public interface TipoCliente {

  public String getRuc();

  default String getPrueba(Persona persona) {
        return persona.dni;
  }

  default String getSaludo() {
    return getPartSaludo() + "Interfaz";
  }

  default String getSaludo2() {
    return getPartSaludo() + "Interfaz";
  }

  private String getPartSaludo() {
    return "Hola soy una ";
  }
}
